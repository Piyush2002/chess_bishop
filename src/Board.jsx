import React from 'react';

export default function Board({ id ,color,isRed}) {
  
  let backgroundColor;
  const row = Math.ceil(id / 8);
  const col = id % 8;
  if((row+col)%2==0){
    backgroundColor= "black";
    // console.log(div_id);
  }
  else backgroundColor="white"

  if(isRed){
    isRed.map((ele)=>{
      if(ele===id) backgroundColor = 'red';
    })
   
  }
  return (
    <>
      <div style={{ height: '100px', width: '100px', backgroundColor}} onClick={()=>color(id)}>{id}</div>
    </>
  );
}
