import { useState } from 'react'
import Board from './Board';

function App() {
  
  let arr=[];
  for(let i=1;i<=64;i++){
    arr.push(i);
  }
  const [isRed,setIsRed]=useState([]);
  function color(id){
    setIsRed([]);
    let temp=[];
    temp.push(id);
    console.log(id);
    let row = Math.floor((parseInt(id) -1) / 8);
    let col = (parseInt(id) -1) % 8;


    let dr = [-1, -1, 1, 1];
    let dc = [1, -1, 1, -1];

    for (let x = 0; x < 4; x++) { 
        let nrow = row + dr[x];
        let ncol = col + dc[x];


        while (nrow >= 0 && nrow < 8 && ncol >= 0 && ncol < 8) {
          temp.push(nrow*8+ncol+1)
          nrow += dr[x];
          ncol += dc[x];
        }
      }
      
      setIsRed(temp);
  }

  return (
    <>
      <section style={{display:'flex',justifyContent:'center',alignItems:'center'}}>

        <div style={{backgroundColor:'pink',height:'800px',width:'800px',display:'flex',flexWrap:'wrap'}}>

          {console.log(arr)}
          {
            arr.map((id,index)=>{
              // console.log(id);
              return (<Board key={index} id={id} color={color} isRed={isRed}/>)
            })

          }
        </div>
      </section>
    </>
  )
}

export default App
